# bbmri-aai-proxy-idp-template

![maintenance status: end of life](https://img.shields.io/maintenance/end%20of%20life/2023)

This project has reached end of life, and has been superseeded by the project [simplesamlphp-module-lsaai](https://gitlab.ics.muni.cz/perun-proxy-aai/simplesamlphp/simplesamlphp-module-lsaai).

## Description

Template for BBMRI AAI Proxy IdP component

## Contribution

This repository uses [Conventional Commits](https://www.npmjs.com/package/@commitlint/config-conventional).

Any change that significantly changes behavior in a backward-incompatible way or requires a configuration change must be marked as BREAKING CHANGE.

### Available scopes:

- theme

## Instalation

```sh
php composer.phar require bbmri/simplesamlphp-module-bbmri
```
